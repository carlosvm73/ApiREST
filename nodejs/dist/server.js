"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const dotenv = require("dotenv");
dotenv.config({ path: '.env.config' });
const app_1 = require("./app");
mongoose.Promise = global.Promise;
// mongoose.createConnection(process.env.MONGODB_URI || process.env.MONGOLAB_URI)
mongoose.connect(process.env.MONGODB_URI || process.env.MONGOLAB_URI);
mongoose.connection.on("error", () => {
    console.log(`MongoDB connection error. Please make sure MongoDB is running.`);
    process.exit(1);
});
app_1.default.listen(app_1.default.get('port'), () => {
    console.log(`App listening on port:${app_1.default.get('port')}`);
});
//# sourceMappingURL=server.js.map