"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const middleware = require("../config/passport");
const ApiCtrl = require("../controllers/api");
const UserCtrl = require("../controllers/user");
const api = express.Router();
// Routes for Users
api.post('/signin', UserCtrl.signin);
api.post('/signup', UserCtrl.signup);
api.get('/logout', middleware.isAuthenticated, UserCtrl.logout);
api.post('/updateProfile', middleware.isAuthenticated, UserCtrl.updateProfile);
api.post('/updatePassword', middleware.isAuthenticated, UserCtrl.updatePassword);
api.post('/deleteAccount', middleware.isAuthenticated, UserCtrl.deleteAccount);
api.get('/reset', middleware.isAuthenticated, UserCtrl.reset);
api.post('/sendResetPasswordEmail', middleware.isAuthenticated, UserCtrl.sendResetPasswordEmail);
// Info of Api
api.get('/help', ApiCtrl.details);
exports.default = api;
//# sourceMappingURL=v1.js.map