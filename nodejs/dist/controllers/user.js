"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const passport = require("passport");
const User = require("../models/User");
exports.signin = (req, res, next) => {
    passport.authenticate('local', (err, user, info) => {
        if (err) {
            return next(err);
        }
        ;
        if (!user) {
            return res.status(400).send({ message: `Email or Password is Invalid.` });
        }
        ;
        req.logIn(user, err => {
            if (err) {
                return next(err);
            }
            ;
            res.status(200).send({ message: `Login successfully` });
        });
    })(req, res, next);
};
exports.signup = (req, res, next) => {
    const newUser = new User.userModel({
        email: req.body.email,
        password: req.body.password,
        profile: {
            name: req.body.name,
            lastname: req.body.lastname,
            dni: req.body.dni,
            position: req.body.position,
            area: req.body.area
        },
        state: req.body.state
    });
    User.userModel.findOne({ email: req.body.email }, function (err, existingUser) {
        if (err) {
            return next(err);
        }
        ;
        if (existingUser) {
            return res.status(400).send({ message: `This email is already registered.` });
        }
        ;
        newUser.save(err => {
            if (err) {
                return next(err);
            }
            ;
            req.logIn(newUser, err => {
                if (err) {
                    return next(err);
                }
                ;
                res.status(200).send({ message: "Successfully created user." });
            });
        });
    });
};
exports.logout = (req, res) => {
    req.logout();
    res.send({ message: `Logout successfully` });
};
exports.updateProfile = (req, res, next) => {
    User.userModel.findById(req.user.id, (err, user) => {
        if (err) {
            return next(err);
        }
        user.email = req.body.email || "";
        user.state = req.body.state || "";
        user.profile.name = req.body.name || "";
        user.profile.lastname = req.body.lastname || "";
        user.profile.dni = req.body.dni || "";
        user.profile.position = req.body.position || "";
        user.profile.area = req.body.area || "";
        user.save((err) => {
            if (err) {
                if (err.code === 11000) {
                    return res.status(400).send({ message: `The email address you have entered is already associated with an account.` });
                }
                return next(err);
            }
            res.status(200).send({ message: `Profile information has been updated.` });
        });
    });
};
exports.updatePassword = (req, res, next) => {
    User.userModel.findById(req.user.id, (err, user) => {
        if (err) {
            return next(err);
        }
        user.password = req.body.password;
        user.save(err => {
            res.status(200).send({ message: `Password has been changed.` });
        });
    });
};
exports.deleteAccount = (req, res, next) => {
};
exports.reset = (req, res, next) => {
};
exports.sendResetPasswordEmail = (req, res, next) => {
};
//# sourceMappingURL=user.js.map