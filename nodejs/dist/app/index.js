"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const session = require("express-session");
const bodyParser = require("body-parser");
const mongo = require("connect-mongo");
const passport = require("passport");
const compression = require("compression");
const v1_1 = require("../routes/v1");
const MongoStore = mongo(session);
const app = express();
app.set('port', process.env.PORT || 9000);
app.use(compression());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(session({
    resave: true,
    saveUninitialized: true,
    secret: process.env.SECRET_KEY,
    store: new MongoStore({
        url: process.env.MONGODB_URI || process.env.MONGOLAB_URI,
        autoReconnect: true
    })
}));
app.use(passport.initialize());
app.use(passport.session());
app.use("/api", v1_1.default);
app.use(express.static("public"));
exports.default = app;
//# sourceMappingURL=index.js.map