"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const passport = require("passport");
const passportLocal = require("passport-local");
const passportFacebook = require("passport-facebook");
const passportGoogle = require("passport-google");
const _ = require("lodash");
const User = require("../models/User");
const LocalStrategy = passportLocal.Strategy;
const FacebookStrategy = passportFacebook.Strategy;
const GoogleStrategy = passportGoogle.Strategy;
// passport.serializeUser<any, any>((user, done)=>{
//     done(undefined, user._id);
// });
// passport.serializeUser((user, done)=>{
//     done(undefined, user);
// });
passport.serializeUser((user, done) => {
    done(undefined, user.id);
});
passport.deserializeUser((id, done) => {
    User.userModel.findById(id, (err, user) => {
        done(err, user);
    });
});
passport.use(new LocalStrategy({ usernameField: 'email' }, (email, password, done) => {
    User.userModel.findOne({ email: email.toLowerCase() }, (err, user) => {
        if (err) {
            return done(err);
        }
        if (!user) {
            return done(undefined, false, { message: `Email ${email} not registered.` });
        }
        ;
        user.comparePassword(password, (err, isMatch) => {
            if (err) {
                return done(err);
            }
            if (isMatch) {
                return done(undefined, user);
            }
            return done(undefined, false, { message: 'Invalid email or password.' });
        });
    });
}));
passport.use(new FacebookStrategy({
    clientID: process.env.FACEBOOK_SECRET,
    clientSecret: process.env.FACEBOOK_SECRET,
    callbackURL: '/auth/facebook/callback',
    profileFields: ["name", "email", "link", "locale", "timezone"],
    passReqToCallback: true
}, (req, accessToken, refreshToken, profile, done) => {
    if (req.user) {
        User.userModel.findOne({ facebook: profile.id }, (err, existingUser) => {
            if (err) {
                return done(err);
            }
            if (existingUser) {
                done(err);
            }
            else {
                User.userModel.findById(req.user.id, (err, user) => {
                    if (err) {
                        return done(err);
                    }
                    user.facebook = profile.id;
                    user.tokens.push({ kind: "facebook", accessToken });
                    user.profile.name = user.profile.name || profile.name.givenName;
                    user.profile.lastname = user.profile.lastname || profile.name.familyName;
                    user.profile.image = user.profile.image || `https://graph.facebook.com/${profile.id}/picture?type=large`;
                    user.save((err) => {
                        done(err, user);
                    });
                });
            }
        });
    }
    else {
        User.userModel.findOne({ facebook: profile.id }, (err, existingUser) => {
            if (err) {
                return done(err);
            }
            if (existingUser) {
                done(err);
            }
            else {
                const user = new User.userModel();
                user.email = profile._json.email;
                user.facebook = profile.id;
                user.tokens.push({ kind: "facebook", accessToken });
                user.profile.name = profile.name.givenName;
                user.profile.lastname = profile.name.familyName;
                user.profile.image = `https://graph.facebook.com/${profile.id}/picture?type=large`;
                user.save((err) => {
                    done(err, user);
                });
            }
        });
    }
}));
exports.isAuthenticated = (req, res, next) => {
    if (req.isAuthenticated()) {
        return next();
    }
    res.status(401).send({ message: "You need to authenticate to access this resource." });
};
exports.isAuthorized = (req, res, next) => {
    const provider = req.path.split("/").slice(-1)[0];
    if (_.find(req.user.tokens, { kind: provider })) {
        res.status(401).send({ message: "You need to authorizate to access this resource." });
    }
};
//# sourceMappingURL=passport.js.map