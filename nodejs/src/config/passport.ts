import * as passport from 'passport';
import * as passportLocal from 'passport-local';
import * as passportFacebook from 'passport-facebook';
import * as passportGoogle from 'passport-google';

import { Request, Response, NextFunction } from "express";
import * as _ from 'lodash';

import * as User from '../models/User';

const LocalStrategy = passportLocal.Strategy;
const FacebookStrategy = passportFacebook.Strategy;
const GoogleStrategy = passportGoogle.Strategy;

// passport.serializeUser<any, any>((user, done)=>{
//     done(undefined, user._id);
// });

// passport.serializeUser((user, done)=>{
//     done(undefined, user);
// });
passport.serializeUser<any, any>((user, done) => {
    done(undefined, user.id);
});

passport.deserializeUser((id, done) => {
    User.userModel.findById(id, (err, user) => {
        done(err, user);
    })
})

passport.use(new LocalStrategy({ usernameField: 'email' }, (email, password, done) => {
    User.userModel.findOne({ email: email.toLowerCase() }, (err: Error, user: any) => {
        if (err) { return done(err); }
        if (!user) { return done(undefined, false, { message: `Email ${email} not registered.` }) };
        user.comparePassword(password, (err: Error, isMatch: Boolean) => {
            if (err) { return done(err); }
            if (isMatch) { return done(undefined, user); }
            return done(undefined, false, { message: 'Invalid email or password.' });
        })
    })
}))

passport.use(new FacebookStrategy({
    clientID: process.env.FACEBOOK_SECRET,
    clientSecret: process.env.FACEBOOK_SECRET,
    callbackURL: '/auth/facebook/callback',
    profileFields: ["name", "email", "link", "locale", "timezone"],
    passReqToCallback: true
}, (req, accessToken, refreshToken, profile, done) => {
    if (req.user) {
        User.userModel.findOne({ facebook: profile.id }, (err, existingUser) => {
            if (err) { return done(err); }
            if (existingUser) {
                done(err);
            } else {
                User.userModel.findById(req.user.id, (err, user: any) => {
                    if (err) { return done(err); }
                    user.facebook = profile.id;
                    user.tokens.push({ kind: "facebook", accessToken });
                    user.profile.name = user.profile.name || profile.name.givenName;
                    user.profile.lastname = user.profile.lastname || profile.name.familyName;
                    user.profile.image = user.profile.image || `https://graph.facebook.com/${profile.id}/picture?type=large`;
                    user.save((err) => {
                        done(err, user);
                    });
                })
            }
        })
    } else {
        User.userModel.findOne({ facebook: profile.id }, (err, existingUser) => {
            if (err) { return done(err); }
            if (existingUser) {
                done(err);
            } else {
                const user: any = new User.userModel();
                user.email = profile._json.email;
                user.facebook = profile.id;
                user.tokens.push({ kind: "facebook", accessToken });
                user.profile.name = profile.name.givenName;
                user.profile.lastname = profile.name.familyName;
                user.profile.image = `https://graph.facebook.com/${profile.id}/picture?type=large`;
                user.save((err) => {
                    done(err, user);
                });
            }
        })
    }
}))

export let isAuthenticated = (req: Request, res: Response, next: NextFunction) => {
    if (req.isAuthenticated()) {
        return next();
    }
    res.status(401).send({ message: "You need to authenticate to access this resource." })
}
export let isAuthorized = (req: Request, res: Response, next: NextFunction) => {
    const provider = req.path.split("/").slice(-1)[0];
    if (_.find(req.user.tokens, { kind: provider })) {
        res.status(401).send({ message: "You need to authorizate to access this resource." })
    }
}