import * as mongoose from 'mongoose';
import * as dotenv from 'dotenv';
import * as parh from 'path';

dotenv.config({path:'.env.config'});

import app from './app';
(<any>mongoose).Promise = global.Promise;
// mongoose.createConnection(process.env.MONGODB_URI || process.env.MONGOLAB_URI)
mongoose.connect(process.env.MONGODB_URI || process.env.MONGOLAB_URI)
mongoose.connection.on("error",()=>{
    console.log(`MongoDB connection error. Please make sure MongoDB is running.`);
    process.exit(1);
})

app.listen(app.get('port'),()=>{
    console.log(`App listening on port:${app.get('port')}`);
})