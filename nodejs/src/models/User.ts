import * as mongoose from 'mongoose';
import * as bcrypt from 'bcrypt-nodejs';
import * as crypto from 'crypto';

export type UserModel = mongoose.Document & {
    email: string,
    password: string,
    passwordReset: string,
    passwordExpires: Date,

    facebook: string,
    google: string,
    tokens: AuthToken[],

    status: boolean,
    state: boolean,

    profile: {
        name: string,
        lastname: string,
        dni: number,
        image: string,
        position: string,
        area: string
    },
    comparePassword:(candidatePassword: string, cb: (err: any, isMatch:any)=>{})=>void,
    gravatar:(size:number)=>string
}

export type AuthToken = {
    accessToken: string,
    kind: string
}

const Schema = mongoose.Schema;

const UserSchema = new Schema({
    email: { type: String, unique: true, lowercase: true, required: true },
    password: { type: String, required: true },
    passwordReset: String,
    passwordExpires: Date,

    facebook: String,
    google: String,
    tokens: Array,

    profile: {
        name: { type: String, required: true },
        lastname: { type: String, required: true },
        dni: { type: Number, unique: true, required: true, len:8 },
        image: String,
        position: String,
        area: String
    },

    status: Boolean,
    state: Boolean
},{timestamps:true});

UserSchema.pre('save',function(next){
    const user = this;
    if(!user.isModified("password")){ return next();}
    bcrypt.genSalt(10, (err, salt)=>{
        if(err) { return next(err); }
        bcrypt.hash(user.password, salt, undefined, (err:mongoose.Error, hash)=>{
            if(err){ return next(err)};
            user.password = hash;
            next();
        })
    })
})

UserSchema.methods.comparePassword = function(candidatePassword:string, cb:(err:any, isMatch:any)=>{}){
    bcrypt.compare(candidatePassword, this.password,(err:mongoose.Error, isMatch:boolean)=>{
        cb(err, isMatch);
    })
}

UserSchema.methods.gravatar = function(size:number){
    if(!size){
        size = 200;
    }
    if(!this.email){
        return `https://gravatar.com/avatar/?s=${size}&d=retro`;
    }
    const md5 = crypto.createHash("md5").update(this.email).digest("hex");
    return `https://gravatar.com/avatar/${md5}?s=${size}&=retro`;
}

export let userModel = mongoose.model("users", UserSchema);