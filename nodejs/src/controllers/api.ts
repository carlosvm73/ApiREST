export let details = (req, res) => {
    res.status(200).send({
        "user": [
            { uri: "/api/signin", type: "post", params: [], body: ["email", "password"], description: "Credenciales requeridas Email y Password." },
            { uri: "/api/signup", type: "post", params: [], body: ["email", "password", "name", "lastname", "dni", "position", "area", "state"], description: "Requiere de estos campos para poder crear un nuevo Usuario; y al mismo tiempo inicia session." },
            { uri: "/api/logout", type: "get", params: [], body: [], description: "la sesiones recupeardas desde la base de datos son borradas." },
            { uri: "/api/updateProfile", type: "post", params: [], body: [], description: "." },
            { uri: "/api/updatePassword", type: "post", params: [], body: [], description: "." },
            { uri: "/api/deleteAccount", type: "post", params: [], body: [], description: "." },
            { uri: "/api/reset", type: "get", params: [], body: [], description: "." },
            { uri: "/api/sendResetPasswordEmail", type: "post", params: [], body: [], description: "." },
        ],
        "position": {

        }
    });
};
