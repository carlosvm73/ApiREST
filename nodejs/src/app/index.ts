import * as express from 'express';
import * as session from 'express-session';
import * as bodyParser from 'body-parser';
import * as mongoose from 'mongoose';
import * as mongo from 'connect-mongo';
import * as passport from 'passport';
import * as compression from 'compression';
import routes from '../routes/v1';

const MongoStore = mongo(session);

const app = express();

app.set('port',process.env.PORT || 9000);
app.use(compression());
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(session({
    resave:true,
    saveUninitialized:true,
    secret:process.env.SECRET_KEY,
    store: new MongoStore({
        url:process.env.MONGODB_URI || process.env.MONGOLAB_URI,
        autoReconnect:true
    })
}));
app.use(passport.initialize());
app.use(passport.session());
app.use("/api", routes);
app.use(express.static("public"));



export default app;