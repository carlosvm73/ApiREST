import * as express from 'express';
import * as middleware from '../config/passport';

import * as ApiCtrl from '../controllers/api';
import * as UserCtrl from '../controllers/user';

const api = express.Router();
// Routes for Users
api.post('/signin', UserCtrl.signin);
api.post('/signup', UserCtrl.signup);
api.get('/logout', middleware.isAuthenticated, UserCtrl.logout);
api.post('/updateProfile',middleware.isAuthenticated,UserCtrl.updateProfile);
api.post('/updatePassword',middleware.isAuthenticated,UserCtrl.updatePassword);
api.post('/deleteAccount',middleware.isAuthenticated,UserCtrl.deleteAccount);
api.get('/reset',middleware.isAuthenticated,UserCtrl.reset);
api.post('/sendResetPasswordEmail',middleware.isAuthenticated,UserCtrl.sendResetPasswordEmail);
// Info of Api
api.get('/help',ApiCtrl.details);
export default api;

